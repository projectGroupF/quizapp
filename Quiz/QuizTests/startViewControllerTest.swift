//
//  startViewControllerTest.swift
//  QuizTests
//
//  Created by Sam Zammit on 4/12/19.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import XCTest
@testable import Quiz


class startViewControllerTest: XCTestCase {
    var startViewController: StartViewController!
    
    override func setUp() {
        super.setUp()
     
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: StartViewController = storyboard.instantiateViewController(withIdentifier: "startViewController") as! StartViewController
        startViewController = vc
        _ = vc.view
    }
    
    func testMainMenuSetNameButton() {
        XCTAssertNil(startViewController.customView, "CustomView was already loaded.")
        startViewController.setNameButton.sendActions(for: .touchUpInside)
        XCTAssertNotNil(startViewController.customView, "CustomView hasn't been loaded.")
    }
    
    func testCustomNameSetting() {
        var submitButton: UIButton? = nil
        var nameField: UITextField? = nil
        
        XCTAssertNotNil(startViewController.setNameButton, "Set Name Button hasn't been loaded.")
        startViewController.setNameButton.sendActions(for: .touchUpInside)
        XCTAssertNotNil(startViewController.customView, "CustomView hasn't been loaded.")
        print(startViewController.customView.subviews)
        submitButton = startViewController.customView.subviews[0] as? UIButton
        nameField = startViewController.customView.subviews[1] as? UITextField
        XCTAssertNotNil(submitButton, "SubmitButton hasn't been loaded.")
        XCTAssertNotNil(nameField, "NameField hasn't been loaded.")
        nameField?.text = "John Doe"
        submitButton?.sendActions(for: .touchUpInside)
        XCTAssert(UserDefaults.standard.string(forKey: "playerName") == "John Doe")
    }
    

}
