//
//  QuizTests.swift
//  QuizTests
//
//  Created by Ludvig Angenfelt on 2019-12-03.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import XCTest
@testable import Quiz

class QuizTests: XCTestCase {
    
    
    func testPlayerClassFunctions() {
        let player = Player()
        
        player.loseLife()
        player.loseLife()
        XCTAssert(player.getStats()?.life == 1)
        
        player.addLife()
        XCTAssert(player.getStats()?.life == 2)
    }
    
    func testRuleEngineClassFunction1() {
        let newPlayer = Player()
        let ruleEngine = RuleEngine(_player: newPlayer)
        let answer = ruleEngine.correctAnswere(userAnswer: "no", correctAnswer: "yes", time: 5)
        
        XCTAssert(answer == false)
    }
    
    func testRuleEngineClassFunction2() {
        let newPlayer = Player()
        newPlayer.loseLife()
        newPlayer.loseLife()
        newPlayer.loseLife()
        let ruleEngine = RuleEngine(_player: newPlayer)
        
        XCTAssert(ruleEngine.checkIfLose() == true)
    }

    func testChangeThemeColor() {
        UserDefaults.standard.set(UIColor.green, forKey: "themeColor")
        XCTAssert(UserDefaults.standard.color(forKey: "themeColor") == UIColor.green)
        
        UserDefaults.standard.set(UIColor.black, forKey: "themeColor")
        XCTAssert(UserDefaults.standard.color(forKey: "themeColor") != UIColor.green)
        
        UserDefaults.standard.set(UIColor.yellow, forKey: "themeColor")
        XCTAssert(UserDefaults.standard.color(forKey: "themeColor") == UIColor.yellow)
    }
    
}
