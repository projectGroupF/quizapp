//
//  ViewController.swift
//  Quiz
//
//  Created by Daniel Ekeroth on 2019-11-12.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import UIKit
import GameKit
class StartViewController: UIViewController  {
    func showAchievements() {
        let vc = GKGameCenterViewController()
        vc.gameCenterDelegate = self
        vc.viewState = .achievements
        present(vc, animated: true, completion: nil)
    }
    
    func showLeaderboard(){
        let gcVC = GKGameCenterViewController()
        gcVC.gameCenterDelegate = self
        gcVC.viewState = .leaderboards
        gcVC.leaderboardIdentifier = "grp.highscoresingleplayer"
        present(gcVC, animated: true, completion: nil)
    }
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var leaderboardButton: UIButton!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet var customView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        GameCenterHelper.helper.viewController = self
        //Hides navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if let _ = UserDefaults.standard.color(forKey: "themeColor"){
            for case let button as UIButton in self.view.subviews {
                button.backgroundColor = UserDefaults.standard.color(forKey: "themeColor")
            }
        } else {
            UserDefaults.standard.set(UIColor.black, forKey: "themeColor")
        }
    }

    @IBAction func playButton(_ sender: Any) {
        performSegue(withIdentifier: "mode", sender: self)
    }
    
    @IBAction func achievements(_ sender: Any) {
        showAchievements()
    }
    @IBAction func leaderboardButton(_ sender: Any) {
        showLeaderboard()
    }
    
    @IBAction func settingsButton(_ sender: Any) {
        performSegue(withIdentifier: "settings", sender: self)
    }
    
    @IBAction func submitAction(sender:UIButton){
        self.customView.isHidden = true
        var name : String?
        for case let textField as UITextField in self.customView.subviews {
            name = textField.text
        }
        UserDefaults.standard.set(name, forKey: "playerName")
        self.customView.endEditing(true)
    }
}

//MARK: UserDefaults extension to set theme color

extension StartViewController: GKGameCenterControllerDelegate{
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
}


extension UserDefaults {

    func color(forKey key: String) -> UIColor? {

        guard let colorData = data(forKey: key) else { return nil }

        do {
            return try NSKeyedUnarchiver.unarchivedObject(ofClass: UIColor.self, from: colorData)
        } catch let error {
            print("color error \(error.localizedDescription)")
            return nil
        }

    }

    func set(_ value: UIColor?, forKey key: String) {

        guard let color = value else { return }
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: color, requiringSecureCoding: false)
            set(data, forKey: key)
        } catch let error {
            print("error color key data not saved \(error.localizedDescription)")
        }

    }

}

