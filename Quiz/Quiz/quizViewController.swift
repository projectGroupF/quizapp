//
//  quizViewController.swift
//  Quiz
//
//  Created by Daniel Ekeroth on 2019-11-12.
//  Copyright © 2019 ProjectF. All rights reserved.
//


//MARK: TODO

//send notification when players turn
//Ui showing if win or lose


import UIKit
import AVFoundation
import QuartzCore
//MARK: Declare structs
struct Question : Codable{
    let category : String
    let type : String
    let difficulty : String
    let question : String
    let correct_answer : String
    let incorrect_answers : [String]
}

struct DATA : Codable{
    let response_code : Int
    let results : [Question]
}

struct x_y{
    let _x : Double
    let _y : Double
    
    init(x : Double, y: Double) {
        _x = x
        _y = y
    }
}

import GameKit

class quizViewController: UIViewController{
    
    //MARK: Declare variables, some of them outlets
    var gameData : gameInfo?
    var player: Player!
    var ruleEngine: RuleEngine!
    var questionClass: QuestionClass!
    var timePassed: Float = 0
    var timeMax: Float = 15
    var timer = Timer()
    @IBOutlet var customView: UIView!
    let dispatchGroup = DispatchGroup()
    @IBOutlet weak var questionField: UITextView!
    var currentQuestionIndex : Int = 0
    var qArr: [Question] = []
    var currentQuestion: Question?
    var buttonsActive: [UIButton] = []
    var timebarDefault : UILabel!
    var correctSoundEffect: AVAudioPlayer?
    private var isSendingTurn = false
    public var model : gameModel?
    @IBOutlet weak var live1: UIImageView!
    @IBOutlet weak var live2: UIImageView!
    @IBOutlet weak var live3: UIImageView!
    
    @IBOutlet weak var streakLabel: UILabel!
    
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var timerBar: UIProgressView!
    //MARK: Check if player is correct or not and executes necessary functions
    @IBAction func checkAnswer(sender:UIButton){
        timer.invalidate()
        guard !isSendingTurn && GameCenterHelper.helper.canTakeTurnForCurrentMatch else {
            return
        }
        let userAnswer: String = sender.titleLabel!.text!
        
        disableButtons()
        model?.question = nil
        if ruleEngine.correctAnswere(userAnswer: userAnswer, correctAnswer: textFix(theString: currentQuestion!.correct_answer), time: timeMax - timePassed) {
            sender.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            playSound(path: "correctSoundEffect", ext: "wav")
            addStreakAnimation()
            if (gameData?.mode != "Solo"){
                model!.players[model!.currentPlayer]! += 1
                GameCenterHelper.helper.endTurn(model!) { error in
                      defer {
                        self.isSendingTurn = false
                      }
                      if let e = error {
                        print("Error ending turn: \(e.localizedDescription)")
                        return
                      }
                }
                if ((model?.checkWin())!){
                   GameCenterHelper.helper.win { error in
                       defer {
                         self.isSendingTurn = false
                       }
                       if let e = error {
                         print("Error winning match: \(e.localizedDescription)")
                         return
                       }
                       self.returnToMenu()
                   }
                } else {
                    model?.question = nil
                      model?.category = gameData!.category
                      model?.difficulty = gameData!.difficulty
                      model?.shift()
                      GameCenterHelper.helper.endTurn(model!) { error in
                          defer {
                            self.isSendingTurn = false
                          }

                          if let e = error {
                            print("Error ending turn: \(e.localizedDescription)")
                            return
                          }
                          
                          self.returnToMenu()
                    }
                }
            }
        }
        else {
            sender.backgroundColor = #colorLiteral(red: 1, green: 0.2112955879, blue: 0.155250383, alpha: 1)
            playSound(path: "wrongSoundEffect", ext: "wav")
            resetStreakAnimation()
            for button in buttonsActive {
                if button.titleLabel?.text == currentQuestion?.correct_answer {
                    button.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                }
            }
            if gameData?.mode != "Solo"{
                model?.question = nil
                model?.category = gameData!.category
                model?.difficulty = gameData!.difficulty
                model?.shift()
                  GameCenterHelper.helper.endTurn(model!) { error in
                  defer {
                    self.isSendingTurn = false
                  }

                  if let e = error {
                    print("Error ending turn: \(e.localizedDescription)")
                    return
                  }
                  
                  self.returnToMenu()
                }
            }
        }
        
        if gameData?.mode == "Solo" {
            checkLose()
            showLives()
        } else {
            GameCenterHelper.helper.endTurn(model!) { error in
              defer {
                self.isSendingTurn = false
              }

              if let e = error {
                print("Error ending turn: \(e.localizedDescription)")
                return
              }
              
              self.returnToMenu()
            }
        }
        showStreak()
    }
    
    
    //MARK: Load view
    override func viewDidLoad() {
        guard !isSendingTurn && GameCenterHelper.helper.canTakeTurnForCurrentMatch else {
            return performSegue(withIdentifier: "gamestats", sender: self)
        }
        super.viewDidLoad()
        if gameData?.mode != "Solo" {
            self.questionClass = QuestionClass(difficulty: model!.difficulty, category: model!.category)
            print(model?.players)
        } else {
            self.questionClass = QuestionClass(difficulty: gameData!.difficulty, category: gameData!.category)
        }
        
        //self.timerBar.backgroundColor = UserDefaults.standard.color(forKey: "themeColor")
        self.player = Player()
        self.ruleEngine = RuleEngine(_player: self.player)
        applyQuestion()
    }
    //MARK: Prepare to send game's stats for game-over view/screen
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? gameoverViewController {
            destination.playerStats = player.getStats()
        } else if let destination = segue.destination as? GameStatsViewController {
            destination.gamestats = self.model
        }
        
    }
    //MARK: Time-related functions
    private func outOfTime(){
        if (timePassed >= timeMax) {
            disableButtons()
            playSound(path: "wrongSoundEffect", ext: "wav")
            resetStreakAnimation()
            for button in self.buttonsActive{
                if (button.currentTitle == textFix(theString: currentQuestion!.correct_answer)) {
                    button.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                }
            }
            self.ruleEngine.wrongAnswer()
            showLives()
            showStreak()
            checkLose()
        }
    }
    
    private func toggleTimer(){
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction() {
        self.timePassed += 0.01
        self.outOfTime()
        self.timerBar.progress -= (0.01 / timeMax)
    }
    
    private func restartTimer(){
        self.timePassed = 0
        self.timerBar.progress = 1
        toggleTimer()
    }
    //MARK: Check game state
    private func checkLose() {
        timer.invalidate()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            if self.ruleEngine.checkIfLose() {
                self.performSegue(withIdentifier: "gameOver", sender: self)
            } else {
                self.applyQuestion()
            }
        })
    }

    @IBAction func submitAction(sender:UIButton){
        self.customView.isHidden = true
        var name : String?
        for case let textField as UITextField in self.customView.subviews {
            name = textField.text
        }
        UserDefaults.standard.set(name, forKey: "playerName")
        self.player.setName(_n: name!)
        self.customView.endEditing(true)
    }
    
    //MARK: Apply questions and guesses and clear previous screen 
    func applyQuestion(){
        self.questionClass.setQuestion()
        self.questionClass.dispatchGroup.notify(queue: .main){
            self.restartTimer()
            self.removeSubview()
            if (self.model?.question != nil) {
                self.currentQuestion = self.model?.question
            } else {
                self.currentQuestion = self.questionClass.currentQuestion
            }
            self.model?.question = self.currentQuestion
            var arrayNumbers : [Int] = []
            let buttonW : Double = Double(self.buttonView.frame.width / 2.25)
            let buttonH : Double = Double(self.buttonView.frame.height / 2.25)
            let locations : [x_y] = [x_y(x : 0, y: 0), x_y(x : Double(self.buttonView.frame.width) - buttonW, y: 0),x_y(x : 0, y: Double(self.buttonView.frame.height) - buttonH), x_y(x : Double(self.buttonView.frame.width) - buttonW, y: Double(self.buttonView.frame.height) - buttonH)]
            var t = 0
            for (index, _) in self.currentQuestion!.incorrect_answers.enumerated(){
                arrayNumbers.append(index)
                t = index + 1
            }
            arrayNumbers.append(t)
            for item in self.currentQuestion!.incorrect_answers{
                let indexNumber = Int.random(in: 0 ... arrayNumbers.count-1)
                self.buttonView.addSubview(self.makeButtonWithText(text: item, x:locations[arrayNumbers[indexNumber]]._x, y:locations[arrayNumbers[indexNumber]]._y, w: buttonW, h: buttonH))
                    arrayNumbers.remove(at: indexNumber)
                }
    
            self.questionField.text = self.textFix(theString: self.currentQuestion!.question)
            
            self.buttonView.addSubview(self.makeButtonWithText(text: self.currentQuestion!.correct_answer, x:locations[arrayNumbers[0]]._x, y:locations[arrayNumbers[0]]._y, w: buttonW, h: buttonH))
            print("The right answer to the question is: \(self.currentQuestion!.correct_answer)")
        }
        //self.currentQuestionIndex += 1
        
    }
    
    func removeSubview(){
        for _ in self.buttonsActive{
            if let viewWithTag = self.buttonView.viewWithTag(100) {
                viewWithTag.removeFromSuperview()
                self.buttonsActive.removeFirst()
            }
        }
    }

    func makeButtonWithText(text:String, x: Double, y: Double, w: Double, h: Double ) -> UIButton {
        let myButton = UIButton(type: .system)
        myButton.frame = CGRect(x: x, y: y, width: w, height: h)
        myButton.backgroundColor = UserDefaults.standard.color(forKey: "themeColor")
        myButton.setTitle(textFix(theString: text), for: .normal)
        let font1 = UIFont(name:"Courier", size: 22)
        myButton.titleLabel!.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: font1!)
        myButton.titleLabel?.adjustsFontForContentSizeCategory = true
        myButton.setTitleColor(UIColor.white, for: .normal)
        //Set background color
        myButton.addTarget(self,action: #selector(checkAnswer),for: .touchUpInside)
        myButton.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
        myButton.titleLabel?.textAlignment = NSTextAlignment.center
        myButton.tag = 100
        self.buttonsActive.append(myButton)
        return myButton
    }
    
    func disableButtons() {
        for buttons in buttonsActive {
            buttons.isEnabled = false
        }
    }
    
    //MARK: Display lives, streak, special characters correctly
    func showLives() {
        let lives = self.player.getStats()?.life
        live3.isHidden = true
        live2.isHidden = true
        live1.isHidden = true
        if lives == 3 {
            live3.isHidden = false
            live2.isHidden = false
            live1.isHidden = false
        }
        else if lives == 2 {
            live1.isHidden = false
            live2.isHidden = false
        }
        else if lives == 1 {
            live1.isHidden = false
        }
    }
    
    func showStreak() {
        guard let streak = self.player.getStats()?.streak else {return}
        self.streakLabel.text = "Streak: \(streak)"
    }
    
    func textFix(theString: String) -> String {
        var newString : String = theString
        let codeStrings : [String] = ["&quot;","&#039;","&eacute;","&amp;"]
        let replaceStrings : [String] = ["\"", "\'", "e´", "&"]
        for (index,str) in codeStrings.enumerated() {
            if newString.contains(str) {
                newString = newString.replacingOccurrences(of: str, with: replaceStrings[index], options: .literal, range: nil)
            }
        }
        return newString
    }
    
    //MARK: Animations
    
    func resetStreakAnimation() {
        UIView.animateKeyframes(withDuration: 1, delay: 0, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.5) {
                self.streakLabel.frame.origin.y = self.streakLabel.frame.origin.y + 15
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
                self.streakLabel.textColor = UIColor.black
                self.streakLabel.frame.origin.y = self.streakLabel.frame.origin.y - 15
            }

        })
    }
    
    func addStreakAnimation(){
        UIView.animateKeyframes(withDuration: 1, delay: 0, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.5) {
                self.streakLabel.textColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
                self.streakLabel.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            }

            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
                self.streakLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        
        })
    }
    
    //MARK: Sounds
    private func playSound(path : String, ext: String){
        do {
            let path = Bundle.main.path(forResource: path + "." + ext, ofType:nil)!
            let url = URL(fileURLWithPath: path)
            correctSoundEffect = try AVAudioPlayer(contentsOf: url)
            correctSoundEffect?.volume = 1
            correctSoundEffect?.play()
        } catch {
            print("Error loading file")
        }
    }
    
    func returnToMenu(){
        _ = navigationController?.popToRootViewController(animated: true)
    }

}


