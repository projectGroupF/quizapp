//
//  SettingsViewController.swift
//  Quiz
//
//  Created by Oskar Eek on 2019-11-19.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var blackButton: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var yellowButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    
    //MARK: Buttons for theme colors
    @IBAction func blackButton(_ sender: Any) {
        UserDefaults.standard.set(blackButton.backgroundColor, forKey: "themeColor")
        homeButton.tintColor = UserDefaults.standard.color(forKey: "themeColor")
        setBorder(button: blackButton)
        removeBorders(exception: blackButton)
    }
    
    @IBAction func blueButton(_ sender: Any) {
        UserDefaults.standard.set(blueButton.backgroundColor, forKey: "themeColor")
        homeButton.tintColor = UserDefaults.standard.color(forKey: "themeColor")
        setBorder(button: blueButton)
        removeBorders(exception: blueButton)
    }
    
    @IBAction func yellowButton(_ sender: Any) {
        UserDefaults.standard.set(yellowButton.backgroundColor, forKey: "themeColor")
        homeButton.tintColor = UserDefaults.standard.color(forKey: "themeColor")
        setBorder(button: yellowButton)
        removeBorders(exception: yellowButton)
    }
    
    @IBAction func backButton(_ sender: Any) {
        performSegue(withIdentifier: "startViewSeague", sender: self)
    }
    //MARK: load view
    override func viewDidLoad() {
        super.viewDidLoad()
        self.blackButton.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.blueButton.backgroundColor = #colorLiteral(red: 0.08087454205, green: 0.20202098, blue: 0.8672077396, alpha: 1)
        self.yellowButton.backgroundColor = #colorLiteral(red: 0.9024651878, green: 0.861974821, blue: 0, alpha: 1)
        
        for case let button as UIButton in self.stackView.subviews {
            button.layer.cornerRadius = 10
        }
        
        if let theme = UserDefaults.standard.color(forKey: "themeColor") {
            homeButton.tintColor = UserDefaults.standard.color(forKey: "themeColor")
            
            for case let button as UIButton in self.stackView.subviews {
                if button.backgroundColor == theme {
                    setBorder(button: button)
                }
            }
        }
        
    }
    //MARK: Border-related functions
    
    func setBorder(button: UIButton) {
        button.layer.borderWidth = 5
        button.layer.borderColor = UIColor.gray.cgColor
    }
    
    func removeBorders(exception: UIButton) {
        for case let button as UIButton in self.stackView.subviews {
            if button == exception {
                //ignore clicked color button
            }
            else {
                button.layer.borderWidth = 0
            }
        }
    }
    
}

