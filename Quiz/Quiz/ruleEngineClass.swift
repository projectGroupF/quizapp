//
//  ruleEngineClass.swift
//  Quiz
//
//  Created by Oskar Eek on 2019-11-15.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import Foundation

class RuleEngine {
    var player: Player
    
    init(_player: Player) {
        player = _player
    }
    
    func checkIfLose() -> Bool {
        let playerStats = self.player.getStats()
        if playerStats?.life == 0 {
            return true
        }
        return false
    }
    
    func correctAnswere(userAnswer: String, correctAnswer: String, time: Float) -> Bool {
        if userAnswer == correctAnswer {
            rightAnswer(time: time)
            return true
        }
        else {
            wrongAnswer()
            return false
        }
    }
    
    func rightAnswer(time: Float) {
        player.addStreak()
        player.addRightAnswere()
        let playerStats = player.getStats()
        if playerStats?.streak == 6 {
            player.addLife()
        }
        player.addPoints(time: time)
    }
    
    func wrongAnswer() {
        player.resetStreak()
        player.loseLife()
    }
 
}
