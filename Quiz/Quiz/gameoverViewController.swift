//
//  gameoverViewController.swift
//  Quiz
//
//  Created by Daniel Ekeroth on 2019-11-15.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import UIKit
import GameKit

class gameoverViewController: UIViewController {

    @IBOutlet weak var playagainButton: UIButton!
    @IBOutlet weak var scoreField: UILabel!
    @IBOutlet weak var correctAnsfield: UILabel!
    @IBOutlet weak var highestStreakField: UILabel!
    @IBOutlet weak var buttonView: UIView!
    var playerStats : stats?
    var games : [stats] = []
    @IBAction func mainmenuButton(_ sender: Any) {
        performSegue(withIdentifier: "mainmenuSeague", sender: self)
    }
    @IBAction func playagainButton(_ sender: Any) {
        performSegue(withIdentifier: "playagainSeague", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let bestScoreInt = GKScore(leaderboardIdentifier: "grp.highscoresingleplayer")
        bestScoreInt.value = Int64(Float64(playerStats!.points) * 100)
        GKScore.report([bestScoreInt]) { (error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                print("Best Score submitted to your Leaderboard!")
            }
        }
        scoreField.text = String(format: "%.2f", playerStats!.points)
        correctAnsfield.text = playerStats?.rightAnswers.description
        highestStreakField.text = playerStats?.highestStreak.description
        for case let button as UIButton in self.buttonView.subviews {
            button.backgroundColor = UserDefaults.standard.color(forKey: "themeColor")
        }
        // Do any additional setup after loading the view.
    }
}
