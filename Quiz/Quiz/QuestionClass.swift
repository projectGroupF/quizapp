//
//  File.swift
//  Quiz
//
//  Created by Daniel Ekeroth on 2019-11-29.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import UIKit
import Foundation

class QuestionClass {
    private var qArr: [Question] = []
    public let dispatchGroup = DispatchGroup()
    private var currentQuestionIndex : Int = 0
    public var currentQuestion : Question!
    private var stringAdder : String?
    public let categoryList = [9: "General Knowledge", 10: "Entertainment: Books", 11: "Entertainment: Film", 12: "Entertainment: Music", 14: "Entertainment: Television", 15: "Entertainment: Video Games", 16: "Entertainment: Board Games", 17: "Science & Nature", 18: "Science: Computers", 19: "Science: Mathetmatics", 20: "Mythology", 21: "Sports", 22: "Geography", 23: "History", 24: "Politics",26: "Celebrities", 27: "Animals", 28: "Vehicles", 31: "Entertainment: Japanese Anime & Manga", 32: "Entertainment: Cartoon & Animations"]
    init(difficulty: String, category: String) {
        for (key, value) in categoryList{
            if value == category{
                self.stringAdder = "&category=\(key)&difficulty=\(difficulty.lowercased())"
            }
        }
        fetchQuestions()
    }
    
    public func setQuestion() {
        if self.currentQuestionIndex >= 10{
            self.currentQuestionIndex = 0
            self.fetchQuestions()
        }
        dispatchGroup.notify(queue: .main){
            let question = self.qArr[self.currentQuestionIndex]
            self.currentQuestionIndex += 1
            self.currentQuestion = question
        }
    }
    
    private func fetchQuestions()	{
        self.dispatchGroup.enter()
        let url = URL(string: "https://opentdb.com/api.php?amount=10" + self.stringAdder!)!
        print(url)
        let task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let jsonDict = try decoder.decode(DATA.self, from: data)
                    self.qArr = jsonDict.results
                    self.dispatchGroup.leave()
                } catch let parseErr {
                    print("JSON Parsing Error", parseErr)
                   }
            }
        })
        task.resume()
    }
}
