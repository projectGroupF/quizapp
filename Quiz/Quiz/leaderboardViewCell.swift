//
//  leaderboardViewCell.swift
//  Quiz
//
//  Created by Ludvig Angenfelt on 2019-11-12.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import UIKit

class leaderboardViewCell: UITableViewCell {
    

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var pointsLabel: UILabel!
    
    @IBOutlet weak var answersLabel: UILabel!
    
    @IBOutlet weak var streakLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
