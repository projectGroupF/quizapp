//
//  model.swift
//  Quiz
//
//  Created by Ludvig Angenfelt on 2019-11-12.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import Foundation


struct leaderboardCell {
    let name: String
    let points: String
    let rightAnswers: String
    let highestStreak: String
}
