//
//  GameStatsViewController.swift
//  Quiz
//
//  Created by daniel ekeroth on 2019-12-29.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import UIKit

class GameStatsViewController: UIViewController {
    @IBOutlet weak var player1Label: UILabel!
    @IBOutlet weak var player2Label: UILabel!
    var gamestats : gameModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        player1Label.text = String(gamestats!.players["p1"]!)
        player2Label.text = String(gamestats!.players["p2"]!)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: Any) {
        _ = navigationController?.popToRootViewController(animated: true)

    }
    
}
