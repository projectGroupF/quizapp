//
//  Carousel.swift
//  Quiz
//
//  Created by daniel ekeroth on 2019-12-29.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import Foundation

var modeList : [mode] = [mode(image: UIImage(named: "example.jpg")!, name: "Solo", description: "Play a relaxing game with yourself on the road or fight for the the top spot in the leaderboard!"), mode(image: UIImage(named: "example.jpg")!, name: "Duel", description: "Play a relaxing game with yourself on the road or fight for the the top spot in the leaderboard!"), mode(image: UIImage(named: "example.jpg")!, name: "Party", description: "Play a relaxing game with yourself on the road or fight for the the top spot in the leaderboard!") ]

public func numberOfItems(in carousel: iCarousel) -> Int {
    return modeList.count
}

public func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
    let data : mode = modeList[index]
    var itemView = UIView()
    itemView = UIView(frame: CGRect(x: 0, y: 0, width: _width, height: _height))
    itemView.contentMode = .scaleToFill
    let button = UIButton(type: .custom)
    button.frame = CGRect(x: 0, y: 0, width: _width, height: _height)
    button.backgroundColor = UIColor.clear
    button.setImage(data.image, for: .normal)
    let label = UILabel(frame: CGRect(x:0,y:Int(Float(_height) / 3.5),width:_width,height:200))
    label.textAlignment = .center
    label.text = data.name
    label.numberOfLines = 0
    label.lineBreakMode = .byCharWrapping
    label.font = UIFont(name: "helvetica", size: 37)
    button.setTitle(data.name, for: .normal)
    button.setTitleColor(UIColor.clear, for: .normal)
    button.tag = carousel.currentItemIndex
    button.addTarget(self, action: #selector(buttonNextAction), for: .touchUpInside)
    itemView.addSubview(button)
    itemView.addSubview(label)

    return itemView
}
