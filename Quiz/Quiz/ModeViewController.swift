//
//  ModeViewController.swift
//  Quiz
//
//  Created by daniel ekeroth on 2019-12-28.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import UIKit
import GameKit

struct mode {
    var image: UIImage
    var name: String
    var description: String
}

struct gameInfo{
    var mode: String
    var difficulty: String
    var category: String
    
    init() {
        mode = ""
        difficulty = ""
        category = ""
    }
}

class ModeViewController: UIViewController , iCarouselDataSource, iCarouselDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    var prepIndex : Int = 1
    var _width : Int = 240
    var _height : Int = 400
    var newgame: Bool = false
    var newGameModel: gameModel?
    var newMatch: GKTurnBasedMatch?
    var dataToTransfer : gameInfo = gameInfo()
    public let categoryList = [9: "General Knowledge", 10: "Entertainment: Books", 11: "Entertainment: Film", 12: "Entertainment: Music", 14: "Entertainment: Television", 15: "Entertainment: Video Games", 16: "Entertainment: Board Games", 17: "Science & Nature", 18: "Science: Computers", 19: "Science: Mathetmatics", 20: "Mythology", 21: "Sports", 22: "Geography", 23: "History", 24: "Politics",26: "Celebrities", 27: "Animals", 28: "Vehicles", 31: "Entertainment: Japanese Anime & Manga", 32: "Entertainment: Cartoon & Animations"]
    var modeList : [mode] = [mode(image: UIImage(named: "example.jpg")!, name: "Solo", description: "Play a relaxing game with yourself on the road or fight for the the top spot in the leaderboard!"), mode(image: UIImage(named: "example.jpg")!, name: "Duel", description: "Play a relaxing game with yourself on the road or fight for the the top spot in the leaderboard!"), mode(image: UIImage(named: "example.jpg")!, name: "Party", description: "Play a relaxing game with yourself on the road or fight for the the top spot in the leaderboard!") ]
    @IBOutlet weak var modeView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        createCarousel()
    }

    func numberOfItems(in carousel: iCarousel) -> Int {
        return modeList.count
    }

    @IBAction func backToMenu(_ sender: Any) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let data : mode = modeList[index]
        var itemView = UIView()
        itemView = UIView(frame: CGRect(x: 0, y: 0, width: _width, height: _height))
        itemView.contentMode = .scaleToFill
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: _width, height: _height)
        button.backgroundColor = UIColor.clear
        button.setImage(data.image, for: .normal)
        let label = UILabel(frame: CGRect(x:0,y:Int(Float(_height) / 3.5),width:_width,height:200))
        label.textAlignment = .center
        label.text = data.name
        label.numberOfLines = 0
        label.lineBreakMode = .byCharWrapping
        label.font = UIFont(name: "helvetica", size: 37)
        button.setTitle(data.name, for: .normal)
        button.setTitleColor(UIColor.clear, for: .normal)
        button.tag = carousel.currentItemIndex
        button.addTarget(self, action: #selector(buttonNextAction), for: .touchUpInside)
        itemView.addSubview(button)
        itemView.addSubview(label)

        return itemView
    }
    
    @objc func buttonNextAction(sender: UIButton) {
        if let viewWithTag = self.view.viewWithTag(1000) {
            viewWithTag.removeFromSuperview()
        }
        if prepIndex == 1{
            self.dataToTransfer.mode = sender.titleLabel!.text!
            if self.dataToTransfer.mode == "Party"{
                _ = navigationController?.popViewController(animated: true)
                return
            }
            GameCenterHelper.helper.presentMatchmaker(mode: self.dataToTransfer.mode)
            onlineGame()
            self.modeList = [mode(image: UIImage(named: "example.jpg")!, name: "Easy", description: self.modeList[sender.tag].description), mode(image: UIImage(named: "example.jpg")!, name: "Medium", description: self.modeList[sender.tag].description), mode(image: UIImage(named: "example.jpg")!, name: "Hard", description: self.modeList[sender.tag].description) ]
            titleLabel.text = "Difficulty"
            
            createCarousel()

        } else if prepIndex == 2{
            self.dataToTransfer.difficulty = sender.titleLabel!.text!
            self.modeList = []
            for (_, value) in self.categoryList {
                self.modeList.append(mode(image: UIImage(named: "example.jpg")!, name: value, description: value))
            }
            titleLabel.text = "Category"
            createCarousel()

        } else if prepIndex == 3 && GameCenterHelper.isAuthenticated{
            self.dataToTransfer.category = sender.titleLabel!.text!
            if dataToTransfer.mode == "Duel" {
                self.newGameModel!.category = self.dataToTransfer.category
                self.newGameModel!.difficulty = self.dataToTransfer.difficulty
                loadAndDisplay(match: self.newMatch!)
            } else{
                performSegue(withIdentifier: "quizGame", sender: self)
            }
        } 
        self.prepIndex += 1
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? quizViewController {
            destination.gameData = dataToTransfer
            if (dataToTransfer.mode != "Solo"){
                destination.model = sender as? gameModel
            }
        }
    }
    
    func createCarousel(){
        let selfWidth : CGFloat = self.view.frame.width
        let selfHeight : CGFloat = self.view.frame.height
        let carousel = iCarousel(frame: CGRect(x: 0, y: selfHeight / 2 - 90, width: selfWidth, height: 180))
        carousel.dataSource = self
        carousel.type = .rotary
        carousel.tag = 1000
        self.view.addSubview(carousel)
    }
    @objc private func presentGame(_ notification: Notification) {
      // 1
      guard let match = notification.object as? GKTurnBasedMatch else {
        return
      }
        self.newMatch = match
      loadAndDisplay(match: match)
    }
    
    func onlineGame(){
        GameCenterHelper.helper.currentMatch = nil

        NotificationCenter.default.addObserver(
          self,
          selector: #selector(presentGame(_:)),
          name: .presentGame,
          object: nil
        )
    }

    // MARK: - Helpers

    private func loadAndDisplay(match: GKTurnBasedMatch) {
      // 2
      match.loadMatchData { data, error in
        var model: gameModel
        self.newgame = false
        if self.newGameModel != nil {
            self.startGame(match: match, model: self.newGameModel!)
        }
        if let data = data {
          do {
            // 3
            model = try JSONDecoder().decode(gameModel.self, from: data)

          } catch {
            self.newgame = true
            model = gameModel()
            self.newGameModel = model
          }
        } else {
          self.newgame = true
          model = gameModel()
          self.newGameModel = model
        }
        
        if self.newgame == false {
            self.startGame(match: match, model: model)
        }
        // 4
      }
    }
    private func startGame(match: GKTurnBasedMatch, model: gameModel){
        GameCenterHelper.helper.currentMatch = match
        self.performSegue(withIdentifier: "quizGame", sender: model)
    }
}
