//
//  leaderboardViewController.swift
//  Quiz
//
//  Created by Ludvig Angenfelt on 2019-11-12.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import UIKit


class leaderboardViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var homeButton: UIButton!
    
    var rounds: [stats] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        self.tableView.rowHeight = 44
        
        if let _ = UserDefaults.standard.color(forKey: "themeColor"){
            homeButton.tintColor = UserDefaults.standard.color(forKey: "themeColor")
            headerView.backgroundColor = UserDefaults.standard.color(forKey: "themeColor")
        }
        
        if let data = UserDefaults.standard.value(forKey:"allGamesSaved") as? Data {
           let bestRounds = try? PropertyListDecoder().decode(Array<stats>.self, from: data)

           for n in bestRounds! {
               rounds.append(n)
           }
        }
        
        tableView.reloadData()
    }
    
    
    @IBAction func toMenuButton(_ sender: Any) {
        performSegue(withIdentifier: "leaderboardToMenu", sender: self)
    }
    
}

//MARK: table- and cell-specific functions
extension leaderboardViewController: UITableViewDelegate, UITableViewDataSource {
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rounds.count
    }
    
    //remove section header in leaderboard
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let scoreCell = tableView.dequeueReusableCell(withIdentifier: "leaderboardCell", for: indexPath) as? leaderboardViewCell {            
            let leaderboard = rounds[indexPath.row]
            scoreCell.nameLabel.text = leaderboard.name
            scoreCell.pointsLabel.text = String(format: "%.2f", leaderboard.points)
            scoreCell.answersLabel.text = String(leaderboard.rightAnswers)
            scoreCell.streakLabel.text = String(leaderboard.highestStreak)

            return scoreCell
        }
        
        return leaderboardViewCell()
    }
}
