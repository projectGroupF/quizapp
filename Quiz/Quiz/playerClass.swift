//
//  roundClass.swift
//  Quiz
//
//  Created by user161035 on 11/13/19.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import Foundation

struct stats : Codable {
    var name : String
    var rightAnswers : Int
    var life : Int
    var points : Float
    var streak : Int
    var highestStreak : Int
    
    init() {
        rightAnswers = 0
        life = 3
        points = 0.0
        streak = 0
        name = ""
        highestStreak = 0
    }
}

class Player {
    var roundStats = stats()
    
    func addRightAnswere() {
        self.roundStats.rightAnswers += 1
    }
    
    func addStreak() {
        self.roundStats.streak += 1
        if self.roundStats.streak > self.roundStats.highestStreak {
            self.roundStats.highestStreak = self.roundStats.streak
        }
    }
    
    func resetStreak() {
        self.roundStats.streak = 0
    }
    
    func addLife() {
        if self.roundStats.life < 3 {
            self.roundStats.life += 1
        }
    }
    
    func setName(_n : String){
        self.roundStats.name = _n
    }
    
    func loseLife() {
        self.roundStats.life -= 1
    }
    
    func addPoints(time: Float) {
        self.roundStats.points += time * 0.8
        //maybe need to change 0.1 depending on if mili-seconds or seconds
    }
    
    func getStats() -> stats? {
        return roundStats
    }
}
