//
//  Gamemodel.swift
//  Quiz
//
//  Created by daniel ekeroth on 2019-12-29.
//  Copyright © 2019 ProjectF. All rights reserved.
//

import Foundation
import GameKit
struct gameModel: Codable {
    var p1: Int
    var p2: Int
    var category: String
    var difficulty: String
    var players : [String: Int] = ["p1" : 0, "p2": 0]
    var question: Question?
    var currentPlayer: String {
      return isKnightTurn ? "p1" : "p2"
    }
    
    var currentOpponent: String {
      return isKnightTurn ? "p2" : "p1"
    }
    private(set) var isKnightTurn: Bool
    
    init() {
            p1 = 0
            p2 = 0
            difficulty = "Easy"
            category = "General Knowledge"
            isKnightTurn = true
    }
    
    mutating public func shift(){
        isKnightTurn = !isKnightTurn
    }
    
    func checkWin() -> Bool{
        if players[currentPlayer] == 3{
            return true
        } else {
            return false
        }
    }
    
}
